﻿using System.Data;
using Data;
using Entities;

namespace Controllers
{
    public class ClientController
    {
        DbConnection connection = new DbConnection();

        //Deleting Clients
        public string Delete(Clients clients)
        {
            return connection.Command($"delete from Clients where clientId = '{clients.ClientId}'");
        }

        //Updating Clients
        public string Update(Clients clients)
        {
            return connection.Command($"update Clients set lastName = '{clients.LastName}', clientName = '{clients.ClientName}', " +
                $"clientAddress = '{clients.ClientAddress}', clientPhone = '{clients.ClientPhone}' where clientId = '{clients.ClientId}'");
        }

        //Showing Clients
        public DataSet Show(string query, string table)
        {
            return connection.Show(query, table);
        }
    }
}
