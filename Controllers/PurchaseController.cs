﻿using System.Data;
using Data;
using Entities;

namespace Controllers
{
    public class PurchasesController
    {
        DbConnection connection = new DbConnection();

        //Deleting Purchases
        public string Delete(Purchases purchases)
        {
            return connection.Command($"delete from Purchases where purchaseId = '{purchases.PurchaseId}'");
        }

        //Updating Purchases
        public string Update(Purchases purchases)
        {
            return connection.Command($"update Purchases set clientfk = '{purchases.ClientFk}', productfk = '{purchases.ProductFk}'," +
                $"quantity = '{purchases.Quantity}', cost = '{purchases.Cost}' where purchaseId = '{purchases.PurchaseId}'");
        }

        //Showing Purchases
        public DataSet Show(string query, string table)
        {
            return connection.Show(query, table);
        }
    }
}
