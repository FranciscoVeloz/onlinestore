﻿using System.Data;
using Data;
using Entities;

namespace Controllers
{
    public class ProviderController
    {
        DbConnection connection = new DbConnection();

        //Inserting providers
        public string Insert(Providers providers)
        {
            return connection.Command($"insert into Providers values(null, '{providers.Business}', '{providers.ProviderName}', " +
                $"'{providers.ProviderAddress}', '{providers.ProviderPhone}')");
        }

        //Deleting providers
        public string Delete(Providers providers)
        {
            return connection.Command($"delete from Providers where providerId = '{providers.ProviderId}'");
        }

        //Updating providers
        public string Update(Providers providers)
        {
            return connection.Command($"update Providers set business = '{providers.Business}', providerName = '{providers.ProviderName}'," +
                $" providerAddress = '{providers.ProviderAddress}', providerPhone = '{providers.ProviderPhone}' " +
                $"where providerId = '{providers.ProviderId}'");
        }

        //Showing providers
        public DataSet Show(string query, string table)
        {
            return connection.Show(query, table);
        }
    }
}
