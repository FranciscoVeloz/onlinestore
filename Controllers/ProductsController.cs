﻿using System.Data;
using System.Windows.Forms;
using Data;
using Entities;

namespace Controllers
{
    public class ProductsController
    {
        DbConnection connection = new DbConnection();

        //Inserting products
        public string Insert(Products products)
        {
            return connection.Command($"insert into Products values(null, '{products.Code}', '{products.Description}', '{products.Price}', " +
                $"'{products.CategoryFk}', '{products.ProviderFk}')");
        }

        //Deleting products
        public string Delete(Products products)
        {
            return connection.Command($"delete from Products where productId = '{products.ProductId}'");
        }

        //Updating products
        public string Update(Products products)
        {
            return connection.Command($"update Products set code = '{products.Code}', description = '{products.Description}', price = '{products.Price}', " +
                $"categoryfk = '{products.CategoryFk}', providerfk = '{products.ProviderFk}' where productId = '{products.ProductId}'");
        }

        //Showing products
        public DataSet Show(string query, string table)
        {
            return connection.Show(query, table);
        }

        //Filling Combo box
        public void FillCategory(ComboBox combo, string query, string tabla)
        {
            combo.DataSource = connection.Show(query, tabla).Tables[0];
            combo.DisplayMember = "categoryId";
        }

        //Filling Combo box
        public void FillProvider(ComboBox combo, string query, string tabla)
        {
            combo.DataSource = connection.Show(query, tabla).Tables[0];
            combo.DisplayMember = "providerId";
        }
    }
}
