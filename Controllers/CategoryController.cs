﻿using System.Data;
using Data;
using Entities;

namespace Controllers
{
    public class CategoryController
    {
        DbConnection connection = new DbConnection();

        //Inserting categories
        public string Insert(Categories categories)
        {
            return connection.Command($"insert into Categories values(null, '{categories.Category}')");
        }

        //Deleting categories
        public string Delete(Categories categories)
        {
            return connection.Command($"delete from Categories where categoryId = '{categories.CategoryId}'");
        }

        //Updating categories
        public string Update(Categories categories)
        {
            return connection.Command($"update Categories set category = '{categories.Category}' where categoryId = '{categories.CategoryId}'");
        }

        //Showing categories
        public DataSet Show(string query, string table)
        {
            return connection.Show(query, table);
        }
    }
}
