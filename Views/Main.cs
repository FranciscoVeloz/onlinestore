﻿using System;
using System.Windows.Forms;

namespace Views
{
    public partial class Main : Form
    {
        public int state = 0;
        public Main()
        {
            InitializeComponent();
        }

        private void OptCategory_Click(object sender, EventArgs e)
        {
            Category_Frm category = new Category_Frm();
            category.ShowDialog();
        }

        private void OptSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OptProvider_Click(object sender, EventArgs e)
        {
            Provider_Frm provider = new Provider_Frm();
            provider.ShowDialog();
        }

        private void OptProduct_Click(object sender, EventArgs e)
        {
            Products_frm products = new Products_frm();
            products.ShowDialog();
        }

        private void OptClient_Click(object sender, EventArgs e)
        {
            Client_frm client = new Client_frm();
            client.ShowDialog();
        }

        private void OptPurchase_Click(object sender, EventArgs e)
        {
            Purchase_frm purchase = new Purchase_frm();
            purchase.ShowDialog();
        }
    }
}
