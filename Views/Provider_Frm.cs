﻿using System;
using System.Windows.Forms;
using Controllers;
using Entities;

namespace Views
{
    public partial class Provider_Frm : Form
    {
        ProviderController controller = new ProviderController();
        Providers providers = new Providers(0, "", "", "", "");
        int id = 0;
        public Provider_Frm()
        {
            InitializeComponent();
        }

        private void BtnReturn_Click(object sender, EventArgs e)
        {
            Close();
        }

        void UpdateDtg()
        {
            ProviderDtg.DataSource = controller.Show($"select * from Providers", "Providers").Tables[0];
            ProviderDtg.AutoResizeColumns();
            ClearTxt();
            TxtBusiness.Focus();
        }

        void ClearTxt()
        {
            TxtBusiness.Clear();
            TxtName.Clear();
            TxtAddress.Clear();
            TxtPhone.Clear();
        }

        private void Provider_Frm_Load(object sender, EventArgs e)
        {
            UpdateDtg();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            string result = controller.Insert(new Providers(0, TxtBusiness.Text, TxtName.Text, TxtAddress.Text, TxtPhone.Text));
            UpdateDtg();
        }

        private void ProviderDtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            providers.ProviderId = int.Parse(ProviderDtg.Rows[e.RowIndex].Cells[0].Value.ToString());
            providers.Business = ProviderDtg.Rows[e.RowIndex].Cells[1].Value.ToString();
            providers.ProviderName = ProviderDtg.Rows[e.RowIndex].Cells[2].Value.ToString();
            providers.ProviderAddress = ProviderDtg.Rows[e.RowIndex].Cells[3].Value.ToString();
            providers.ProviderPhone = ProviderDtg.Rows[e.RowIndex].Cells[4].Value.ToString();

            id = providers.ProviderId;
            TxtBusiness.Text = providers.Business;
            TxtName.Text = providers.ProviderName;
            TxtAddress.Text = providers.ProviderAddress;
            TxtPhone.Text = providers.ProviderPhone;
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            string newResult = controller.Update(providers = new Providers(id, TxtBusiness.Text, TxtName.Text, TxtAddress.Text, TxtPhone.Text));
            UpdateDtg();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Do you want to delete the provider: {providers.ProviderName}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                controller.Delete(providers);
                UpdateDtg();
            }
        }
    }
}
