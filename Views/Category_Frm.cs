﻿    using System;
using System.Windows.Forms;
using Controllers;
using Entities;

namespace Views
{
    public partial class Category_Frm : Form
    {
        CategoryController controller = new CategoryController();
        Categories categories = new Categories(0, "");
        int id = 0;
        public Category_Frm()
        {
            InitializeComponent();
        }
        private void BtnReturn_Click(object sender, EventArgs e)
        {
            Close();
        }

        void UpdateDtg()
        {
            CategoryDtg.DataSource = controller.Show($"select * from Categories", "Categories").Tables[0];
            CategoryDtg.AutoResizeColumns();
            TxtCategory.Clear();
            TxtCategory.Focus();
        }

        private void Category_Frm_Load(object sender, EventArgs e)
        {
            UpdateDtg();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            string result = controller.Insert(new Categories(0, TxtCategory.Text));
            UpdateDtg();
        }
        private void CategoryDtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            categories.CategoryId = int.Parse(CategoryDtg.Rows[e.RowIndex].Cells[0].Value.ToString());
            categories.Category = CategoryDtg.Rows[e.RowIndex].Cells[1].Value.ToString();

            id = categories.CategoryId;
            TxtCategory.Text = categories.Category;
        }
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            string newResult = controller.Update(categories = new Categories(id, TxtCategory.Text));
            UpdateDtg();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Do you want to delete the category: {categories.Category}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                controller.Delete(categories);
                UpdateDtg();
            }

        }
    }
}
