﻿using System;
using System.Windows.Forms;
using Controllers;
using Entities;

namespace Views
{
    public partial class Purchase_frm : Form
    {
        PurchasesController controller = new PurchasesController();
        Purchases purchases = new Purchases(0, 0, 0, 0, 0);
        int id = 0;
        public Purchase_frm()
        {
            InitializeComponent();
        }
        private void BtnReturn_Click(object sender, EventArgs e)
        {
            Close();
        }
        void UpdateDtg()
        {
            PurchaseDtg.DataSource = controller.Show($"select * from Purchases", "Purchases").Tables[0];
            PurchaseDtg.AutoResizeColumns();
            TxtClient.Clear();
            TxtCost.Clear();
            TxtDate.Clear();
            TxtProduct.Clear();
            TxtQuantity.Clear();
        }

        private void Purchase_frm_Load(object sender, EventArgs e)
        {
            UpdateDtg();
        }

        private void PurchaseDtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            purchases.PurchaseId = int.Parse(PurchaseDtg.Rows[e.RowIndex].Cells[0].Value.ToString());
            purchases.ClientFk = int.Parse(PurchaseDtg.Rows[e.RowIndex].Cells[1].Value.ToString());
            purchases.ProductFk = int.Parse(PurchaseDtg.Rows[e.RowIndex].Cells[2].Value.ToString());
            purchases.Quantity = int.Parse(PurchaseDtg.Rows[e.RowIndex].Cells[3].Value.ToString());
            purchases.Cost = double.Parse(PurchaseDtg.Rows[e.RowIndex].Cells[4].Value.ToString());
            purchases.PurchaseDate = PurchaseDtg.Rows[e.RowIndex].Cells[5].Value.ToString();

            id = purchases.PurchaseId;
            TxtClient.Text = purchases.ClientFk.ToString();
            TxtProduct.Text = purchases.ProductFk.ToString();
            TxtQuantity.Text = purchases.Quantity.ToString();
            TxtCost.Text = purchases.Cost.ToString();
            TxtDate.Text = purchases.PurchaseDate;
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            string newResult = controller.Update(purchases = new Purchases(id, int.Parse(TxtClient.Text), int.Parse(TxtProduct.Text), int.Parse(TxtQuantity.Text), double.Parse(TxtCost.Text)));
            UpdateDtg();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Do you want to delete the purchase: {purchases.PurchaseId}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                controller.Delete(purchases);
                UpdateDtg();
            }
        }
    }
}
