﻿using System;
using System.Windows.Forms;
using Controllers;
using Entities;

namespace Views
{
    public partial class Products_frm : Form
    {
        ProductsController controller = new ProductsController();
        Products products = new Products(0, "", "", "", 0, 0);
        int id = 0;
        public Products_frm()
        {
            InitializeComponent();
        }

        private void BtnReturn_Click(object sender, EventArgs e)
        {
            Close();
        }

        void UpdateDtg()
        {
            controller.FillCategory(CmbCategory, "Select categoryId from Categories", "Categories");
            controller.FillProvider(CmbProvider, "Select providerId from Providers", "Providers");
            ProductsDtg.DataSource = controller.Show($"select * from Products", "Products").Tables[0];
            ProductsDtg.AutoResizeColumns();

            TxtCode.Clear();
            TxtDescription.Clear();
            TxtPrice.Clear();
            CmbCategory.Text = "";
            CmbProvider.Text = "";
            TxtCode.Focus();
        }

        private void Products_frm_Load(object sender, EventArgs e)
        {
            UpdateDtg();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            string result = controller.Insert(new Products(0, TxtCode.Text, TxtDescription.Text, TxtPrice.Text, 
                int.Parse(CmbCategory.Text), int.Parse(CmbProvider.Text)));
            UpdateDtg();
        }

        private void ProductsDtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            products.ProductId = int.Parse(ProductsDtg.Rows[e.RowIndex].Cells[0].Value.ToString());
            products.Code = ProductsDtg.Rows[e.RowIndex].Cells[1].Value.ToString();
            products.Description = ProductsDtg.Rows[e.RowIndex].Cells[2].Value.ToString();
            products.Price = ProductsDtg.Rows[e.RowIndex].Cells[3].Value.ToString();
            products.CategoryFk = int.Parse(ProductsDtg.Rows[e.RowIndex].Cells[4].Value.ToString());
            products.ProviderFk = int.Parse(ProductsDtg.Rows[e.RowIndex].Cells[5].Value.ToString());

            id = products.ProductId;
            TxtCode.Text = products.Code;
            TxtDescription.Text = products.Description;
            TxtPrice.Text = products.Price;
            CmbCategory.Text = products.CategoryFk.ToString();
            CmbProvider.Text = products.ProviderFk.ToString();
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            string newResult = controller.Update(products = new Products(id, TxtCode.Text, TxtDescription.Text, TxtPrice.Text,
                int.Parse(CmbCategory.Text), int.Parse(CmbProvider.Text)));
            UpdateDtg();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Do you want to delete the product: {products.Code}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                controller.Delete(products);
                UpdateDtg();
            }
        }
    }
}
