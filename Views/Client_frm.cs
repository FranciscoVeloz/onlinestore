﻿using System;
using System.Windows.Forms;
using Controllers;
using Entities;


namespace Views
{
    public partial class Client_frm : Form
    {
        ClientController controller = new ClientController();
        Clients clients = new Clients(0, "", "", "", "");
        int id = 0;
        public Client_frm()
        {
            InitializeComponent();
        }

        private void BtnReturn_Click(object sender, EventArgs e)
        {
            Close();
        }

        void ClearTxt()
        {
            TxtLast.Clear();
            TxtName.Clear();
            TxtAddress.Clear();
            TxtPhone.Clear();
        }

        void UpdateDtg()
        {
            ClientDtg.DataSource = controller.Show($"select * from Clients", "Clients").Tables[0];
            ClientDtg.AutoResizeColumns();
            ClearTxt();
            TxtLast.Focus();
        }

        private void Client_frm_Load(object sender, EventArgs e)
        {
            UpdateDtg();
        }

        private void ClientDtg_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            clients.ClientId = int.Parse(ClientDtg.Rows[e.RowIndex].Cells[0].Value.ToString());
            clients.LastName = ClientDtg.Rows[e.RowIndex].Cells[1].Value.ToString();
            clients.ClientName = ClientDtg.Rows[e.RowIndex].Cells[2].Value.ToString();
            clients.ClientAddress = ClientDtg.Rows[e.RowIndex].Cells[3].Value.ToString();
            clients.ClientPhone = ClientDtg.Rows[e.RowIndex].Cells[4].Value.ToString();

            id = clients.ClientId;
            TxtLast.Text = clients.LastName;
            TxtName.Text = clients.ClientName;
            TxtAddress.Text = clients.ClientAddress;
            TxtPhone.Text = clients.ClientPhone;
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            string newResult = controller.Update(clients = new Clients(id, TxtLast.Text, TxtName.Text, TxtAddress.Text, TxtPhone.Text));
            UpdateDtg();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Do you want to delete the client: {clients.ClientName}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                controller.Delete(clients);
                UpdateDtg();
            }
        }
    }
}
