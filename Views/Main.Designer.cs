﻿namespace Views
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.OptCategory = new System.Windows.Forms.ToolStripButton();
            this.OptProvider = new System.Windows.Forms.ToolStripButton();
            this.OptProduct = new System.Windows.Forms.ToolStripButton();
            this.OptClient = new System.Windows.Forms.ToolStripButton();
            this.OptPurchase = new System.Windows.Forms.ToolStripButton();
            this.OptSalir = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptCategory,
            this.OptProvider,
            this.OptProduct,
            this.OptClient,
            this.OptPurchase,
            this.OptSalir});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(69, 620);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // OptCategory
            // 
            this.OptCategory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptCategory.Image = ((System.Drawing.Image)(resources.GetObject("OptCategory.Image")));
            this.OptCategory.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptCategory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptCategory.Name = "OptCategory";
            this.OptCategory.Size = new System.Drawing.Size(66, 68);
            this.OptCategory.Text = "Category";
            this.OptCategory.Click += new System.EventHandler(this.OptCategory_Click);
            // 
            // OptProvider
            // 
            this.OptProvider.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptProvider.Image = ((System.Drawing.Image)(resources.GetObject("OptProvider.Image")));
            this.OptProvider.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptProvider.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptProvider.Name = "OptProvider";
            this.OptProvider.Size = new System.Drawing.Size(66, 68);
            this.OptProvider.Text = "Provider";
            this.OptProvider.Click += new System.EventHandler(this.OptProvider_Click);
            // 
            // OptProduct
            // 
            this.OptProduct.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptProduct.Image = ((System.Drawing.Image)(resources.GetObject("OptProduct.Image")));
            this.OptProduct.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptProduct.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptProduct.Name = "OptProduct";
            this.OptProduct.Size = new System.Drawing.Size(66, 68);
            this.OptProduct.Text = "Product";
            this.OptProduct.Click += new System.EventHandler(this.OptProduct_Click);
            // 
            // OptClient
            // 
            this.OptClient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptClient.Image = ((System.Drawing.Image)(resources.GetObject("OptClient.Image")));
            this.OptClient.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.OptClient.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptClient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptClient.Name = "OptClient";
            this.OptClient.Size = new System.Drawing.Size(66, 68);
            this.OptClient.Text = "Client";
            this.OptClient.Click += new System.EventHandler(this.OptClient_Click);
            // 
            // OptPurchase
            // 
            this.OptPurchase.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptPurchase.Image = ((System.Drawing.Image)(resources.GetObject("OptPurchase.Image")));
            this.OptPurchase.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptPurchase.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptPurchase.Name = "OptPurchase";
            this.OptPurchase.Size = new System.Drawing.Size(66, 68);
            this.OptPurchase.Text = "Purchase";
            this.OptPurchase.Click += new System.EventHandler(this.OptPurchase_Click);
            // 
            // OptSalir
            // 
            this.OptSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptSalir.Image = ((System.Drawing.Image)(resources.GetObject("OptSalir.Image")));
            this.OptSalir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptSalir.Name = "OptSalir";
            this.OptSalir.Size = new System.Drawing.Size(66, 68);
            this.OptSalir.Text = "Exit";
            this.OptSalir.Click += new System.EventHandler(this.OptSalir_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1119, 620);
            this.Controls.Add(this.toolStrip1);
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "Main";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton OptCategory;
        private System.Windows.Forms.ToolStripButton OptProvider;
        private System.Windows.Forms.ToolStripButton OptProduct;
        private System.Windows.Forms.ToolStripButton OptClient;
        private System.Windows.Forms.ToolStripButton OptPurchase;
        private System.Windows.Forms.ToolStripButton OptSalir;
    }
}