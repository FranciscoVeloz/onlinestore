﻿
--Creating database
create database OnlineStore;

--Using database
use OnlineStore;

--Creating tables;
create table Categories
(
	categoryId int primary key auto_increment,
	category varchar(100)
);

create table Providers
(
	providerId int primary key auto_increment,
	business varchar(100),
	providerName varchar(100),
	providerAddress varchar(100),
	providerPhone varchar(100)
);

create table Products
(
	productId int primary key auto_increment,
	code varchar(100),
	description varchar(100),
	price varchar(100),
	categoryfk int,
	providerfk int,
	foreign key(categoryfk) references Categories(categoryId),
	foreign key(providerfk) references Providers(providerId)
);

create table Clients
(
	clientId int primary key auto_increment,
	lastName varchar(100),
	clientName varchar(100),
	clientAddress varchar(100),
	clientPhone varchar(100)
);

create table Purchases
(
	purchaseId int primary key auto_increment,
	clientfk int,
	productfk int,
	quantity int,
	cost double,
	purchaseDate timestamp default current_timestamp,
	foreign key(clientfk) references Clients(clientId),
	foreign key(productfk) references Products(productId)
);

--Describing tables
describe Categories;
describe Providers;
describe Products;
describe Clients;
describe Purchases;

--Inserting into Clients and Purchases

insert into Clients values
(null, 'Veloz', 'Francisco', 'Av. de los angeles', '4741309933'),
(null, 'Reyes', 'Daneli', 'Santa Maria', '3322114455'),
(null, 'asd', 'qwe', 'SazcxMarzxc', '33231234455'),
(null, 'Gonzalez', 'Natalia', 'Agustin Rivera', '4745698877'),
(null, 'Ortega', 'Araceli', 'Lopez Cotilla', '4745695194');

insert into Purchases values
(null, 1, 1, 3, 1500, null),
(null, 2, 3, 1, 15000, null),
(null, 4, 3, 2, 30000, null);