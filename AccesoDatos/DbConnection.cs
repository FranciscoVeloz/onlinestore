﻿using System.Data;
using Bases;

namespace Data
{
    public class DbConnection
    {
        Conectar connect = new Conectar("localhost", "root", "", "OnlineStore");

        //Methods
        public string Command(string query)
        {
            return connect.Comando(query);
        }

        public DataSet Show(string query, string table)
        {
            return connect.Consultar(query, table);
        }
    }
}