﻿namespace Entities
{
    public class Products
    {
		public int ProductId { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string Price { get; set; }
		public int CategoryFk { get; set; }
		public int ProviderFk { get; set; }

		public Products(int productId, string code, string description, string price, int categoryFk, int providerFk) =>
			(ProductId, Code, Description, Price, CategoryFk, ProviderFk) = (productId, code, description, price, categoryFk, providerFk);
	}
}
