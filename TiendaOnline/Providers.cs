﻿namespace Entities
{
    public class Providers
    {
        public int ProviderId { get; set; }
        public string Business { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAddress { get; set; }
        public string ProviderPhone { get; set; }

        public Providers(int providerId, string business, string providerName, string providerAddress, string providerPhone) =>
            (ProviderId, Business, ProviderName, ProviderAddress, ProviderPhone) = (providerId, business, providerName, providerAddress, providerPhone);
    }
}
