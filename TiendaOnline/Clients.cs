﻿namespace Entities
{
    public class Clients
    {
        public int ClientId { get; set; }
        public string LastName { get; set; }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public string ClientPhone { get; set; }

        public Clients(int clientId, string lastName, string clientName, string clientAddress, string clientPhone) =>
            (ClientId, LastName, ClientName, ClientAddress, ClientPhone) = (clientId, lastName, clientName, clientAddress, clientPhone);
    }
}
