﻿namespace Entities
{
    public class Categories
    {
        public int CategoryId { get; set; }
        public string Category { get; set; }

        public Categories(int categoryId, string category) => (CategoryId, Category) = (categoryId, category);
    }
}
