﻿namespace Entities
{
    public class Purchases
    {
		public int PurchaseId { get; set; }
		public int ClientFk { get; set; }
		public int ProductFk { get; set; }
		public int Quantity { get; set; }
		public double Cost { get; set; }
		public string PurchaseDate { get; set; }

		public Purchases(int purchaseId, int clientFk, int productFk, int quantity, double cost) =>
			(PurchaseId, ClientFk, ProductFk, Quantity, Cost) = (purchaseId, clientFk, productFk, quantity, cost);
	}
}
